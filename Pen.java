
public class Pen {
	
	int inkLevel=100;
	String color="Black";
	
	public void openCap() {
		System.out.println("Cap of Pen is Open");
	}
	
	public void write() {
	inkLevel-=5;       // inkLevel=inkLevel-5
	System.out.println("Pen is writing");
	System.out.println("Current Ink Level: "+inkLevel);
	}
	
	public void closeCap() {
		System.out.println("Cap of Pen is closed");
	}
}
